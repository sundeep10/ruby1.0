##reading a file using ruby

# File.open("test.txt").each do |line|
	# puts line
# end


## writing to a file 

File.open("myfile1.txt","w") do |file|
	file.puts "oreva uchiha madara da"
	file.write "no line break"            ##write works as print statement in ruby it writes in the same line 
	file.puts "cool"
end

## if we open a file and write something to it the data will be overwritten
## to append we use "a"

File.open("myfile2.txt","a") do |file|   ##append starts from a new line
	file.puts "oreva uchiha madara da"
	file.puts "line break"            
	file.puts "cool"
end
